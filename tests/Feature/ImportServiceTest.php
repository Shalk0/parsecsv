<?php

namespace Tests\Feature;

use App\Classes\ImportService;
use Ddeboer\DataImport\Reader\CsvReader;
use Symfony\Component\Validator\ConstraintViolation;
use Doctrine\ORM\EntityManager;
use Tests\TestCase;
use Psr\Log\LoggerInterface;


class ImportServiceTest extends TestCase
{
    /**
     * @var ImportService
     */
    private $service;

    /**
     * @var EntityManager
     */
    protected $em;

    public function setUp()
    {
        parent::setUp();
        $this->em = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();
        $this->service = new ImportService($this->em, app(LoggerInterface::class), app('validator'));
    }

    /**
     * Testing import valid csv file
     */
    public function testCsvParseWithoutErrors()
    {
        $csvReader = $this->getReader(__DIR__ . '/TestFiles/ExampleValid.csv');
        $this->em->expects($this->never())->method('flush');
        $result = $this->service->importProducts($csvReader, true);
        $this->assertArraySubset(['errors' => 0], $result);
    }

    /**
     * Testing import csv file with import errors
     */
    public function testCsvParseWithImportErrors()
    {
        $csvReader = $this->getReader(__DIR__ . '/TestFiles/ExampleWithImportErrors.csv');
        $this->em->expects($this->never())->method('flush');
        $result = $this->service->importProducts($csvReader, true);
        $this->assertArraySubset(['errors' => 5], $result);
    }

    /**
     *Testing import csv file with parse errors
     */
    public function testCsvParseWithParseErrors()
    {
        $csvReader = $this->getReader(__DIR__.'/TestFiles/ExampleWithParseErrors.csv');
        $this->em->expects($this->never())->method('flush');
        $result = $this->service->importProducts($csvReader, true);
        $this->assertArraySubset(['errors' => 2], $result);
    }

    /**
     *Testing import csv file with parse and import errors
     */
    public function testCsvParseWithParseAndImportErrors()
    {
        $csvReader = $this->getReader(__DIR__.'/TestFiles/ExampleWithParseAndImportErrors.csv');
        $this->em->expects($this->never())->method('flush');
        $result = $this->service->importProducts($csvReader, true);
        $this->assertArraySubset(['errors' => 7], $result);
    }

    /**
     * Get reader for csv file
     *
     * @param $path
     * @return CsvReader
     */
    private function getReader($path)
    {
        $file = new \SplFileObject($path);
        $csvReader = new CsvReader($file);
        $csvReader->setHeaderRowNumber(0);
        return $csvReader;
    }

    /**
     * Get Constraint Violation
     *
     * @param $message
     * @param null $root
     * @param null $propertyPath
     * @return ConstraintViolation
     */
    protected function getViolation($message, $root = null, $propertyPath = null)
    {
        return new ConstraintViolation($message, $message, array(), $root, $propertyPath, null);
    }


}