<?php

namespace Tests\Feature;

use App\Entities\Product;
use Tests\TestCase;

class TestProduct extends TestCase
{
    /**
     * @var Product
     */
    private $product;

    public function setUp()
    {
        parent::setUp();
        $this->product = new Product();
    }

    /**
     * Testing empty product
     */
    public function testEmptyProduct()
    {
        $product = new Product();

        $this->assertNull($product->getDiscontinued());
        $this->assertNull($product->getProductCost());
        $this->assertNull($product->getDateAdd());
        $this->assertNull($product->getDateDiscontinued());
        $this->assertNull($product->getProductStock());
        $this->assertNull($product->getProductId());
        $this->assertNull($product->getProductCode());
        $this->assertNull($product->getProductDescription());
        $this->assertNull($product->getProductName());
    }

    /**
     * Testing product with data
     */
    public function testProductWithData()
    {
        $productData = [
            'Product Code' => 'PC0001',
            'Product Name' => 'PName1',
            'Product Description' => 'PDescription1',
            'Stock' => '10',
            'Cost' => '199.99'
        ];

        $product = new Product();
        $product->setData($productData);

        $this->assertEquals('PC0001', $product->getProductCode());
        $this->assertEquals('PName1', $product->getProductName());
        $this->assertEquals('PDescription1', $product->getProductDescription());
        $this->assertEquals(10, $product->getProductStock());
        $this->assertEquals(199.99, $product->getProductCost());
        $this->assertNull($product->getDiscontinued());
    }

    /**
     * Testing function isProductLessCostAndStock().
     */
    public function testProductWithLessCostAndStock()
    {
        $product = new Product();
        $product->setProductStock(9);
        $product->setProductCost(10);
        $this->assertFalse($product->isProductLessCostAndStock());
        $product->setProductCost(3);
        $this->assertTrue($product->isProductLessCostAndStock());
    }

    /**
     * Testing function isProductOverCost().
     */
    public function testProductWithOverData()
    {
        $product = new Product();
        $product->setProductCost(999.9);
        $this->assertFalse($product->isProductOverCost());
        $product->setProductCost(1000.01);
        $this->assertTrue($product->isProductOverCost());
    }

    /**
     * Testing validation of product without data
     */
    public function testProductWithoutData()
    {
        $this->assertTrue(count($this->getErrors()) > 0);
    }

    /**
     * Testing validation of product with incorrect and correct data
     */
    public function testProductWithIncorrectData()
    {
        $productData = [
            'Product Code' => 'PC0001',
            'Product Name' => 'PName1',
            'Product Description' => 'PDescription1',
        ];

        $this->product->setData($productData);
        $this->assertTrue(count($this->getErrors()) > 0); //Not valid (stock and cost is empty)
        $this->product->setProductStock('Fail');
        $this->product->setProductCost('Fail');
        $this->assertTrue(count($this->getErrors()) > 0); //Not valid (stock and cost not numeric)
        $this->product->setProductStock('100');
        $this->assertTrue(count($this->getErrors()) > 0); //Not valid (cost not numeric)
        $this->product->setProductCost('100');
        $this->assertFalse(count($this->getErrors()) > 0); //Valid
    }

    /**
     * Get the validate errors
     *
     */
    private function getErrors()
    {
        return app('validator')->make($this->product->getData(), Product::getRules())->messages()->toArray();
    }
}
