<?php

namespace Tests\Feature;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\content\LargeFileContent;
use App\Classes\ImportFactory;
use Ddeboer\DataImport\Reader\CsvReader;
use App\Exceptions\FormatNotFoundException;
use App\Exceptions\FileNotFoundException;

class ImportFactoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Testing get reader of not exists file
     *
     * @throws FormatNotFoundException
     */
    public function testBadFormat()
    {
        $file = 'test.txt';
        $format = 'txt';

        try {
            ImportFactory::getReader($format, $file);
            $this->fail('Must throw FormatNotFoundException');
        } catch(FormatNotFoundException $ex) {

        }
    }

    /**
     * Testing get reader of not exists file
     *
     * @throws FileNotFoundException
     */
    public function testNotExistsFile()
    {
        $file = 'test.csv';
        $format = 'csv';

        try {
            ImportFactory::getReader($format, $file);
            $this->fail('Must throw FileNotFoundException');
        } catch (FileNotFoundException $ex) {

        }
    }

    /**
     * Testing get reader for valid data
     */
    public function testExistsFile()
    {
        $file = $this->getValidFile();
        $format = 'csv';
        $reader = ImportFactory::getReader($format, $file->url());
        $this->assertInstanceOf(CsvReader::class, $reader);
    }

    /**
     * Imulate the file
     *
     * @return $this
     */
    private function getValidFile()
    {
        $root = vfsStream::setup();
        return vfsStream::newFile('test.csv', 0777)->withContent(LargeFileContent::withKilobytes(100))->at($root);
    }
}