<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;

class CommandTest extends TestCase
{
    /**
     * Testing how command execute with invalid format
     */
    public function testExecuteWithBadFormat()
    {
        Artisan::call('command:import', [
            'file'   => 'example.csv',
            'format' => 'testformat',
            '--test' => '--test'
        ]);

        $result = Artisan::output();
        $this->assertEquals('Reader for testformat not found' . PHP_EOL, $result);
    }

    /**
     * Testing how command execute with invalid file
     */
    public function testExecuteWithBadFile()
    {
        Artisan::call('command:import', [
            'file'   => 'NotFoundFile.csv',
            'format' => 'csv',
            '--test' => '--test'
        ]);

        $result = Artisan::output();
        $this->assertEquals('File not found' . PHP_EOL, $result);
    }

    /**
     * Testing how command execute with valid format and file
     */
    public function testExecute()
    {
        Artisan::call('command:import', [
            'file'   => 'tests/Feature/TestFiles/ExampleValid.csv',
            'format' => 'csv',
            '--test' => '--test'
        ]);

        $result = Artisan::output();
        $this->assertEquals('Total: 23 Products. Imported: 23, not imported: 0' . PHP_EOL, $result);
    }
}
