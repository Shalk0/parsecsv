1. Создать БД
2. Запустить команду php artisan migrate
3. Запустить консольную команду для импорта данных из файла csv (тестовый файл example.csv) в БД MySql.

Синтаксис:

command:import file format [--test]

(php artisan command:import example.csv csv)


В классе ImportFactory определяется ридер для определенных форматов.

В классе ImportService валидируются объекты и сохраняются в БД (если не указан --test).

В классе Import (Console\Commands\Import.php) описание консольной команды.

Product (Entities\Product.php) описание сущности и методы работы с ней.

Сервис-провайдер - Providers\ImportServiceProvider.php.

Файл с логами - storage\logs\laravel.log.


Был использован Ddeboer Data Import library.
