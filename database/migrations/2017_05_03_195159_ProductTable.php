<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Product', function (Blueprint $table) {
            $table->increments('productId');
            $table->string('productName', 50);
            $table->string('productDescription', 255);
            $table->string('productCode', 10);
            $table->date('dateAdd');
            $table->date('dateDiscontinued')->nullable();
            $table->integer('productStock');
            $table->float('productCost','');
            $table->timestamp('currentDate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Product');
    }
}
