<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\ImportFactory;
use App\Exceptions\FormatNotFoundException;
use App\Exceptions\FileNotFoundException;

/**
 * Class Import
 *
 */
class Import extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:import
                        {file   : The file name}
                        {format : The files format}
                        {--test : For test}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for import csv to database';

    /**
     * Import constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $format = $this->argument('format');
        $file = $this->argument('file');

        try {
            $reader = ImportFactory::getReader($format, $file);
        } catch (FormatNotFoundException $ex) {
            $this->error('<error>Reader for '. $format. ' not found</error>');
            return;
        } catch (FileNotFoundException $ex) {
            $this->error('<error>File not found</error>');
            return;
        }

        $importService = app('serviceImport');
        $result = $importService->importProducts($reader, $this->option('test'));
        $message = sprintf('Total: %s Products. Imported: %s, not imported: %s', $reader->count(), $result['success'], $result['errors']);
        $this->info($message);
    }
}
