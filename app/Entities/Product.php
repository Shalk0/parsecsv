<?php


namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Product
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="Product", indexes={@ORM\Index(name="ProductCode_idx", columns={"ProductCode"})})
 */
class Product
{
    /**
     * @var int
     * @ORM\Column(name="productId", type="integer", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $productId;

    /**
     * @var string
     * @ORM\Column(name="productName", type="string", length=50)
     */
    protected $productName;

    /**
     * @var string
     * @ORM\Column(name="productDescription", type="string", length=255)
     */
    protected $productDescription;

    /**
     * @var string
     * @ORM\Column(name="productCode", type="string", length=10)
     */
    protected $productCode;

    /**
     * @var DateTime
     * @ORM\Column(name="dateAdd", type="datetime")
     */
    protected $dateAdd;

    /**
     * @var DateTime
     * @ORM\Column(name="dateDiscontinued", type="datetime")
     */
    protected $dateDiscontinued;

    /**
     * @var int
     * @ORM\Column(name="productStock", type="integer")
     */
    protected $productStock;

    /**
     * @var float
     * @ORM\Column(name="productCost", type="float")
     */
    protected $productCost;

    /**
     * @var string|null
     */
    protected $discontinued;

    /**
     * @var DateTime
     * @ORM\Column(name="currentDate", type="datetime")
     */
    protected $currentDate;

    /**
     * @var array
     */
    protected $headers = [
        'Product Code' => 'productCode',
        'Product Name' => 'productName',
        'Product Description' => 'productDescription',
        'Stock' => 'productStock',
        'Cost' => 'productCost',
        'Discontinued' => 'discontinued'
    ];

    /**
     * @param array $data
     */
    public function setData($data = [])
    {
        foreach ($data as $key => $value) {
            $key = $this->keyTransform($key);
            if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }
    }

    /**
     * @return array
     */
    public function getData()
    {
        $data = [];
        foreach ($this as $key => $value) {
            $data[$key] = $value;
        }

        return $data;
    }

    /**
     * Transform the key
     *
     * @param $key
     * @return mixed
     */
    protected function keyTransform($key)
    {
        return (array_key_exists($key, $this->headers)) ? $this->headers[$key] : $key;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function setCurrentDate()
    {
        $this->currentDate = new \DateTime();
    }

    /**
     * Set ProductStock
     *
     * @param $productStock
     * @return $this
     */
    public function setProductStock($productStock)
    {
        $this->productStock = $productStock;
        return $this;
    }

    /**
     * Get ProductStock
     *
     * @return int
     */
    public function getProductStock()
    {
        return $this->productStock;
    }

    /**
     * Set ProductCost
     *
     * @param $productCost
     * @return $this
     */
    public function setProductCost($productCost)
    {
        $this->productCost = $productCost;
        return $this;
    }

    /**
     * Get ProductCost
     *
     * @return float
     */
    public function getProductCost()
    {
        return $this->productCost;
    }

    /**
     * Set Discontinued
     *
     * @param $discontinued
     * @return $this
     */
    public function setDiscontinued($discontinued)
    {
        $this->discontinued = $discontinued;
        return $this;
    }

    /**
     * Get Discontinued
     *
     * @return null|string
     */
    public function getDiscontinued()
    {
        return $this->discontinued;
    }

    /**
     * Get ProductId
     *
     * @return integer
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * set ProductName
     *
     * @param $ProductName
     * @return $this
     */
    public function setProductName($ProductName)
    {
        $this->productName = $ProductName;
        return $this;
    }

    /**
     * Get ProductName
     *
     * @return string
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * Set ProductDescription
     *
     * @param $ProductDescription
     * @return $this
     */
    public function setProductDescription($ProductDescription)
    {
        $this->productDescription = $ProductDescription;
        return $this;
    }

    /**
     * Get ProductDescription
     *
     * @return string
     */
    public function getProductDescription()
    {
        return $this->productDescription;
    }

    /**
     * Set ProductCode
     *
     * @param $ProductCode
     * @return $this
     */
    public function setProductCode($ProductCode)
    {
        $this->productCode = $ProductCode;
        return $this;
    }

    /**
     * Get ProductCode
     *
     * @return string
     */
    public function getProductCode()
    {
        return $this->productCode;
    }

    /**
     * @ORM\PrePersist
     */
    public function setDateAdd()
    {
        $this->dateAdd = new \DateTime();
    }

    /**
     * @return DateTime
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * @ORM\PrePersist
     */
    public function setDateDiscontinued()
    {
        if ($this->discontinued === 'yes') {
            $this->dateDiscontinued = new \DateTime();
        }
    }

    /**
     * @return DateTime
     */
    public function getDateDiscontinued()
    {
        return $this->dateDiscontinued;
    }

    /**
     * @return bool
     */
    public function isProductLessCostAndStock()
    {
        return ($this->productCost < 5 && $this->productStock < 10);
    }

    /**
     * @return bool
     */
    public function isProductOverCost()
    {
        return ($this->productCost > 1000);
    }


    /**
     * Get rules for data
     *
     * @return array
     */
    public static function getRules()
    {
        return [
            'productName' => 'string|min:3|max:50',
            'productDescription' => 'string|min:5|max:255',
            'productCode' => 'string|min:3|max:10',
            'productStock' => 'numeric',
            'productCost' => 'numeric|between:0,1000',
        ];
    }
}
