<?php
namespace App\Classes;

use App\Exceptions\FormatNotFoundException;
use Ddeboer\DataImport\Reader\CsvReader;
use App\Exceptions\FileNotFoundException;

/**
 * Class ImportFactory
 * Class for import products
 */
class ImportFactory
{
    /**
     * @param string $format
     * @param string $target
     * @return CsvReader|null
     * @throws FormatNotFoundException
     */
    public static function getReader($format, $target)
    {
        $instance = null;

        switch ($format) {
            case 'csv':
                $instance = self::getCsvReader($target);
                break;
            default:
                throw new FormatNotFoundException('Format not found');
        }

        return $instance;
    }

    /**
     * @param $file
     * @return CsvReader
     * @throws FileNotFoundException
     */
    protected static function getCsvReader($file)
    {
        try {
            $file = new \SplFileObject($file);
            $instance = new CsvReader($file);
            $instance->setHeaderRowNumber(0);
            return $instance;
        } catch (\Exception $ex) {
            throw new FileNotFoundException();
        }
    }
}