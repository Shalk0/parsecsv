<?php

namespace App\Classes;

use Ddeboer\DataImport\Reader;
use App\Entities\Product;
use Doctrine\ORM\EntityManager;
use Illuminate\Contracts\Logging\Log;
use \Illuminate\Validation\Factory as Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/**
 * Class ImportService
 *
 */
class ImportService
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Log
     */
    protected $logger;

    /**
     * @var Validator
     */
    protected $validatorFactory;

    /**
     * ImportService constructor.
     * @param EntityManager $em
     * @param Log $logger
     * @param Validator $validatorFactory
     */
    public function __construct(EntityManager $em, Log $logger, Validator $validatorFactory)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->validatorFactory = $validatorFactory;
        $this->logger->getMonolog()->pushHandler(new StreamHandler('php://stdout', Logger::INFO));
    }

    /**
     * Main method for importing products
     *
     * @param Reader $reader
     * @param bool $isTest
     * @return array
     */
    public function importProducts(Reader $reader, $isTest = false)
    {
        $result = ['success' => 0, 'errors' => 0];

        if (method_exists($reader, 'hasErrors') && $reader->hasErrors()) {
            $result['errors'] += count($this->getErrors($reader)); //increment count of error product
            $message = sprintf('PARSE ERROR. Lines: %s', implode(', ', array_keys($this->getErrors($reader))));
            $this->logger->error($message);
        }

        foreach ($reader as $row) {
            $product = new Product();
            $product->setData($row);
            $validator = $this->validatorFactory->make($product->getData(), Product::getRules());

            /** validate product of cost and stock */
            $validator->after(function ($validator) use ($product) {
                if ($product->isProductLessCostAndStock()) {
                    $validator->errors()->add('Cost and Stock', 'Product cost less than 5 and product stock less than 10');
                }
            });

            if (!$validator->fails()) {
                $this->em->persist($product);
                $result['success']++; //increment count of success product
            } else {
                $result['errors']++; //increment count of error product
                $message = sprintf('The product %s (code: %s) was not imported. Errors:', $product->getProductName(), $product->getProductCode());
                $this->logger->warning($message, $validator->messages()->toArray());
            }
        }

        if (!$isTest) {
            try {
                $this->em->flush(); //insert product in DataBase
            } catch(\Exception $ex) {
                $result['success'] = 0;
                $result['errors'] = $reader->count();
            }
        }

        return $result;
    }

    /**
     * Get parse error
     *
     * @param Reader $reader
     * @return array
     */
    protected function getErrors(Reader $reader)
    {
        return (method_exists($reader, 'getErrors')) ? $reader->getErrors() : array();
    }

}