<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Classes\ImportService;
use Psr\Log\LoggerInterface;

class ImportServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->serviceImport();
    }

    protected function serviceImport()
    {
        $this->app->bind('serviceImport', function ($app) {
            return new ImportService($app['em'], $app[LoggerInterface::class], $app['validator']);
        });
    }
}
